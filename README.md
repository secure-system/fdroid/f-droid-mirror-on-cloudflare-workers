# F-droid repo mirror (proxy) on ☁️️Cloudflare Workers

Proxying traffic to F-droid's official repository through Clouldflare Workers for speed.

## Range

- Repo: repo index, apks, and images
- Website: any pages on `f-droid.org`

## Warning

This may leak your app downloads to Cloudflare.

## Getting started

1. Install `wrangler: https://developers.cloudflare.com/workers/cli-wrangler/install-update

2. Generate an API token: https://developers.cloudflare.com/workers/cli-wrangler/authentication#generate-tokens

3. Set the following environment variables in GitLab

- `CF_ZONE_ID`
- `CF_API_TOKEN`
- `CF_ACCOUNT_ID`

4. Trigger a CI pipeline. A Cloudflare Worker called `fdroid` will be created in your account.
5. Go to https://dash.cloudflare.com/. Under your website's `Workers` section, add an `HTTP Route` pointing to the `fdroid` service.
### Optionally
- Change the `MIRROR_HOST` in `src/handler.ts` to suite your need.
